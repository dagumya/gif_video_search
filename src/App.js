import React, {Component} from 'react';
import './App.css';
import SearchForm from './Components/SearchForm';
import GifList from './Components/GifList';
import axios from 'axios'

// Main container for the application
// will pass the gifs data to the Giflist that will render it
export default class App extends Component {

    constructor() {
        super();
        this.state = {
            gifs: [], // this contains all our gifs, will change and be updated by components
            loading: true,
        }
    }

    componentDidMount() {
        this.performSearch()

    }

    /*
     Create a function that fetches data and updates the gif state when called
     this will be needed for the search functionality
     */

    performSearch = (query = 'lit') => {
        axios.get(`http://api.giphy.com/v1/gifs/search?q=${query}&limit=24&api_key=bmcIPTyqHKhu8Ga57BNS9BtqfAp3XQKu`) // used ES6 feature template strings
            .then(responseData => { // unlike fetch API where we need to transpose the data to JSON. axios JSON is default
                this.setState({
                    gifs: responseData.data.data,
                    loading: false,
                })
            })
            .catch(err => {
                console.log("Error fetching and parsing data", err)
            })
    };

    render() {
        console.log(this.state.gifs);
        return (
            <div>
                <div className="main-header">
                    <div className="inner">
                        <h1 className="main-title">Gif's Search Feature </h1>
                        <SearchForm onSearch={this.performSearch} />
                    </div>
                </div>
                <div className="main-content">
                    {
                        (this.state.loading)
                        ? <p>Loading....</p>
                        : <GifList data={this.state.gifs}/>
                    }
                </div>
            </div>
        );
    }
}
