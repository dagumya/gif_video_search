# Outline
___
Create a REACT app that uses the GIFY developers API to search and display GIFs. Emphasis is on how to make API calls, understanding which 
component should make the calls and which method should handle the requests. 
**Using axios library to fetch external data**

# How to run
---

git clone `my_repo`  
`cd git_video_search`  
`npm install -g pushstate-server`  
`pushstate-server build`
open `http://localhost:9000`

# Note
___
Project challenge from workshop on teamtreehouse.com. 